from latex_mathml_conversion_utils import ConversionUtils
from IndexFilesControllers.index_files_controller import IndexFilesController
from read_extraction_results import ExtractionReader


def get_list_towrite_location_info(dic_file_info, dic_regions, dic_visual_id):
    # Row format:  formula_id, doc id, page id, formula region id, visual_id, formula region (4 elements)
    lst_result = []
    for formula_id in dic_file_info:
        temp_lst = [formula_id]
        temp_lst.extend(dic_file_info[formula_id].split("$$"))
        temp_lst.append(dic_visual_id[formula_id])
        temp_lst.extend(dic_regions[formula_id])
        lst_result.append(temp_lst)
    return lst_result


def get_list_towrite(dic_representation, dic_visual_id):
    # Row format: formula id, visual id, representation
    result_list = []
    for formula_id in dic_representation:
        temp_lst = [formula_id, dic_visual_id[formula_id], dic_representation[formula_id]]
        result_list.append(temp_lst)
    return result_list


def get_list_towrite_errors(lst_formula_ids_failed_pandoc, lst_formula_ids_failed_latexml):
    # Row format: formula id, type
    result_list = []
    for formula_id in lst_formula_ids_failed_pandoc:
        result_list.append([formula_id, "pandoc"])
    for formula_id in lst_formula_ids_failed_latexml:
        result_list.append([formula_id, "latexml"])
    return result_list


class ConversionPipeline:
    def __init__(self, target):
        # mathseer_pipeline.start_latexml_server()
        self.file_system = IndexFilesController(source_dir=target, write_limit=100000)
        self.last_formula_id = self.file_system.last_formula_id
        self.conversion_utils = ConversionUtils(self.file_system.last_visual_id,
                                                visual_ids_slt=self.file_system.visual_ids_slt,
                                                visual_ids_latex=self.file_system.visual_ids_latex)

    def read_extraction_files_and_write_index(self, lst_file_path):
        """
        This is the main method that takes in lst of TSV files and writes the intermediate index files.
        @param lst_file_path:
        @return:
        """
        # Reading TSV files
        lst_document_id_path, dic_document_id_dic_pages = ExtractionReader.read_list_files(lst_file_path)

        # Extracting Representations and Formulas Information from the information extracted from TSV files
        dic_latex, dic_pml, dic_cml, dic_visual_id, dic_regions, dic_file_info, lst_formula_ids_failed_pandoc, lst_formula_ids_failed_latexml = self.__generating_formulas_information(
            dic_document_id_dic_pages)

        # A set of conversions from dictionaries to lists to prepare records to be written in files
        lst_latex = get_list_towrite(dic_latex, dic_visual_id)
        lst_pml = get_list_towrite(dic_pml, dic_visual_id)
        lst_cml = get_list_towrite(dic_cml, dic_visual_id)
        lst_location = get_list_towrite_location_info(dic_file_info, dic_regions, dic_visual_id)
        lst_errors = get_list_towrite_errors(lst_formula_ids_failed_pandoc, lst_formula_ids_failed_latexml)

        # Writing records
        self.file_system.file_doc_paths.write_records(lst_document_id_path)
        self.file_system.file_formula_location.write_records(lst_location)
        self.file_system.file_formula_cml.write_records(lst_cml)
        self.file_system.file_formula_pml.write_records(lst_pml)
        self.file_system.file_formula_latex.write_records(lst_latex)
        self.file_system.file_failure_log.write_records(lst_errors)

    def __generating_formulas_information(self, dic_document_id_dic_pages):
        """
        This method takes in the TSV files dictionary as input and return a set of dictionaries each containing
        information regarding the intermediate index files.
        @param dic_document_id_dic_pages:
        @return: Latex, Presentation MathML, Content MathML, Visual Ids, Regions, File Info dictionaries
        """
        # Reading extraction files and save info in dictionaries with formula id as the key
        dic_file_info, dic_regions, temp_dic_pmml = self.__processing_extraction_info(dic_document_id_dic_pages)
        print("done reading the information from the TSV files")
        # Pandoc conversion
        # lst_latex_strings is list of latex string and empty if the conversion from PMML to latex fails
        lst_latex_strings, lst_exceptions_pmml_to_latex = ConversionUtils.pmml_to_latex(temp_dic_pmml)

        dic_latex = {list(temp_dic_pmml.keys())[i]: lst_latex_strings[i] for i in
                     range(len(list(temp_dic_pmml.keys())))}

        lst_formula_ids_failed_pandoc = []
        for index in lst_exceptions_pmml_to_latex:
            lst_formula_ids_failed_pandoc.append(list(temp_dic_pmml.keys())[index])
        print("done extracting latex strings")
        # LaTeXML conversion
        print("starting extracting MathMLs")
        print("Number of formulas: " + str(len(lst_latex_strings)))
        lst_pml, lst_cml = ConversionUtils.latex_to_cmml_pmml(lst_latex_strings)
        dic_pml = {list(temp_dic_pmml.keys())[i]: lst_pml[i] for i in range(len(list(temp_dic_pmml.keys())))}
        dic_cml = {list(temp_dic_pmml.keys())[i]: lst_cml[i] for i in range(len(list(temp_dic_pmml.keys())))}
        lst_formula_ids_failed_latexml = []
        for formula_id in dic_pml:
            if dic_pml[formula_id] == "":
                lst_formula_ids_failed_latexml.append(formula_id)

        # Handling Visual Ids
        print("Handling Visual Ids")
        dic_visual_id = {}
        for formula_id in dic_pml:
            if dic_pml[formula_id] != "":
                try:
                    # If getting the SLT string fails it uses the LATEX string
                    visual_id = self.conversion_utils.get_visual_id(string_rep=dic_pml[formula_id], is_slt=True)
                except:
                    visual_id = self.conversion_utils.get_visual_id(string_rep=dic_latex[formula_id], is_slt=False)
            else:
                visual_id = self.conversion_utils.get_visual_id(string_rep=dic_latex[formula_id], is_slt=False)
            dic_visual_id[formula_id] = visual_id

        # Returning information related to each formula in separate dictionary
        return dic_latex, dic_pml, dic_cml, dic_visual_id, dic_regions, dic_file_info, lst_formula_ids_failed_pandoc, lst_formula_ids_failed_latexml

    def __processing_extraction_info(self, dic_document_id_dic_pages):
        """
        The input dictionary is in form of
        dictionary of {document id, dictionary of {page id: dictionary of formulas}}
        This method process this dictionary and extract the informations
        @param dic_document_id_dic_pages:
        @return:
        dic_file_info: formula_id : doc_id $$ page_id $$ formula region id
        dic_regions: formula_id : regions
        dic_pmml: formula_id : presentation mathml
        """
        dic_regions = {}
        dic_file_info = {}
        dic_pmml = {}
        for doc_id in dic_document_id_dic_pages:
            page_dic = dic_document_id_dic_pages[doc_id]
            for page_id in page_dic:
                formula_dic = page_dic[page_id]
                for formula_region_id in formula_dic:
                    pml = formula_dic[formula_region_id][1]
                    regions = formula_dic[formula_region_id][0]
                    dic_regions[self.last_formula_id] = regions
                    dic_file_info[self.last_formula_id] = str(doc_id) + "$$" + str(page_id) + "$$" + str(
                        formula_region_id)
                    dic_pmml[self.last_formula_id] = pml
                    self.last_formula_id += 1
        return dic_file_info, dic_regions, dic_pmml
