from opensearchpy import OpenSearch
from opensearchpy import helpers

host = "localhost"
port = 9200
auth = ("admin", "admin")  # For testing only. Don't store credentials in code.
# ca_certs_path = "/home/bm3302/cert/localhost+3.pem"


class ElasticSearchInterFace:
    """
    Provides commands needed to interact with ElasticSearch
    """

    def __init__(self):
        self.es = OpenSearch(
            hosts=[{"localhost": host, "port": port}],
            http_compress=True,  # enables gzip compression for request bodies
            http_auth=auth,
            # client_cert = client_cert_path,
            # client_key = client_key_path,
            use_ssl=True,
            verify_certs=False,
            ssl_assert_hostname=False,
            ssl_show_warn=False,
            # ca_certs=ca_certs_path,
        )

        if not self.es.ping():
            raise ValueError("Connection failed")

    def get_all_indcies(self):
        return self.es.indices.get_alias("*")

    def create_index(self, index_name):
        index_body = {"settings": {"index": {"number_of_shards": 4}}}
        return self.es.indices.create(index=index_name, body=index_body, ignore=400)

    def delete_index(self, index_name):
        try:
            return self.es.indices.delete(index=index_name, ignore=400)
        except Exception as inst:
            print(type(inst))
            print(inst.args)
            print(inst)
            return None

    def insert_data(self, list_data):
        response = helpers.bulk(self.es, list_data, refresh=True)
        return response

    def read_data(self, dictionary, index_name):
        res = self.es.index(index=index_name, id=1, document=dictionary)
        return res

    def read_top_k_index(self, index_name, k):
        res = self.es.search(
            index=index_name,
            body={"size": k, "query": {"match_all": {}}},
        )
        # print(res)
        return res["hits"]["hits"]

    def insert_vecs(self, dimension, index_name, list_data):
        """
        :param dimension: length of vectors
        :param index_name: name of index
        :param list_data: similar to `tsv_to_elastic_list` in conversion_tools.py
        :return:
        """
        mapping = {
            "settings": {"index": {"knn": True, "knn.space_type": "cosinesimil"}},
            "mappings": {
                "properties": {
                    "formula_vector": {"type": "knn_vector", "dimension": dimension}
                }
            }
        }
        # push mappings to elasticsearch
        self.es.indices.create(index=index_name, ignore=400, body=mapping)
        return helpers.bulk(self.es, list_data, refresh=True)

    def read_top_k_vec(self, index_name, query_vec, k):
        search_body = {
            "size": k,
            "query": {"knn": {"formula_vector": {"vector": query_vec, "k": k}}},
        }

        search_res = self.es.search(index=index_name, body=search_body)
        list_results = search_res['hits']['hits']
        dic_result = {}
        for item in list_results:
            # print(item)
            # input("wait")
            temp_dict = {'PageId': float(item["_source"]["PageId"]), 'DocumentId': item["_source"]["DocumentId"],
                         'FormulaRegionId': float(item["_source"]["FormulaRegionId"]), "Score": float(item['_score'])}

            dic_result[str(item['_id'])] = temp_dict

        return dic_result
