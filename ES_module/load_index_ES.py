import sys
from ES_module.search import SearchViaES
import warnings
warnings.filterwarnings("ignore")


def index(sves, embedding_directory, file_id, embedding_type):
    if embedding_type == 1:
        dir_slt = embedding_directory + "/SLT/"
        sves.load_in_index(dir_slt+str(file_id)+".tsv", sves.index_name_slt)
    elif embedding_type == 2:
        dir_opt = embedding_directory + "/OPT/"
        sves.load_in_index(dir_opt+str(file_id)+".tsv", sves.index_name_opt)
    elif embedding_type == 3:
        dir_slt_type = embedding_directory + "/SLTType/"
        sves.load_in_index(dir_slt_type+str(file_id)+".tsv", sves.index_name_slt_type)
    else:
        dir_opt_type = embedding_directory + "/OPTType/"
        sves.load_in_index(dir_opt_type+str(file_id)+".tsv", sves.index_name_opt_type)


def main():
    embedding_directory = sys.argv[1]
    index_id = sys.argv[2]
    file_id = int(sys.argv[3])
    embedding_type = int(sys.argv[4])
    sves = SearchViaES(index_id)
    index(sves, embedding_directory, file_id, embedding_type)


if __name__ == '__main__':
    main()
