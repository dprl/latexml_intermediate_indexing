import time

from ES_module.conversion_tools import tsv_to_elastic_list
from ES_module.elastic_search_interface import ElasticSearchInterFace
from collections import Counter
import multiprocessing

temp_dic = {
    "<csymbol cd=\"mws\" name=\"qvar\">qvar_$</csymbol>": "",
    "<csymbol cd=\"latexml\">differential-d</csymbol>": "<ci>d</ci>",
    "<csymbol cd=\"latexml\">conditional</csymbol>": "<ci>c</ci>",
    "<csymbol cd=\"mws\" name=\"qvar\">qvar_</csymbol>": "",
}


class SearchViaES:
    def __init__(self, index_id, cft_manager=None):
        self.es_interface = ElasticSearchInterFace()
        self.index_name_slt = "slt_index_" + index_id
        self.index_name_slt_type = "slt_type_index_" + index_id
        self.index_name_opt = "opt_index_" + index_id
        self.index_name_opt_type = "opt_type_index_" + index_id
        self.cft = cft_manager

    def get_all_indicies(self):
        return self.es_interface.get_all_indcies()

    def delete_index(self, index_name):
        self.es_interface.delete_index(index_name)

    def generate_vector_index(self):
        self.es_interface.create_index(self.index_name_slt)
        self.es_interface.create_index(self.index_name_slt_type)
        self.es_interface.create_index(self.index_name_opt)
        self.es_interface.create_index(self.index_name_opt_type)

    def delete_indies(self):
        self.es_interface.delete_index(self.index_name_slt)
        self.es_interface.delete_index(self.index_name_slt_type)
        self.es_interface.delete_index(self.index_name_opt)
        self.es_interface.delete_index(self.index_name_opt_type)

    def load_in_index(self, tsv_file, index_name):
        list_data = tsv_to_elastic_list(tsv_file, index_name)
        # self.es_interface.insert_vecs(300, index_name, list_data)
        chunks = [list_data[x:x + 5000] for x in range(0, len(list_data), 5000)]
        for lst in chunks:
            self.es_interface.insert_vecs(300, index_name, lst)

    def __extract_vector_search(self, is_slt, is_type, representation, top_k, return_list):
        if self.cft is None:
            raise Exception("Cannot extract vectors if no CFT manager provided")
        query_vec = self.cft.get_vector(is_slt, is_type, representation)
        if is_slt and is_type:
            index = self.index_name_slt_type
        elif is_slt:
            index = self.index_name_slt
        elif not is_slt and is_type:
            index = self.index_name_opt_type
        else:
            index = self.index_name_opt
        retrieval_result = self.es_interface.read_top_k_vec(index, query_vec, top_k)
        new_dic = {}

        retrieval_result_sorted_by_rank = sorted(
            retrieval_result.items(),
            key=lambda item: item[1]["Score"], reverse=True)

        for i, res in enumerate(retrieval_result_sorted_by_rank):
            # res is tuple of (formula_id, dict with retrieval info, page, region, etc)
            new_dic[res[0]] = retrieval_result[res[0]]
            new_dic[res[0]]["Score"] = retrieval_result[res[0]]["Score"] / (60+i)
        return_list.append(new_dic)

    def retrival(self, latex_formula_query, top_k):
        time1 = time.time()
        cmml, pmml = self.cft.get_mathml(latex_formula_query)
        for item in temp_dic:
            cmml = cmml.replace(item, temp_dic[item], cmml.count(item))
        # cmml = cmml.replace("<csymbol cd=\"latexml\">differential-d</csymbol>", "<ci>d</ci>")
        # print(cmml)
        # input("wait")
        time2 = time.time()
        time_representation_extraction = time2-time1
        manager = multiprocessing.Manager()
        list_results = manager.list()
        jobs = []
        time1 = time.time()
        p = multiprocessing.Process(target=self.__extract_vector_search, args=(True, True, pmml, top_k, list_results))
        jobs.append(p)
        p.start()
        p = multiprocessing.Process(target=self.__extract_vector_search, args=(True, False, pmml, top_k, list_results))
        jobs.append(p)
        p.start()
        p = multiprocessing.Process(target=self.__extract_vector_search, args=(False, True, cmml, top_k, list_results))
        jobs.append(p)
        p.start()
        p = multiprocessing.Process(target=self.__extract_vector_search, args=(False, False, cmml, top_k, list_results))
        jobs.append(p)
        p.start()
        for proc in jobs:
            proc.join()

        combined_retrieval_result = {}
        for repr_results_map in list_results:
            for formula_id in repr_results_map.keys():
                if formula_id in combined_retrieval_result.keys():
                    combined_retrieval_result[formula_id]["Score"] = \
                        combined_retrieval_result[formula_id]["Score"] + repr_results_map[formula_id]["Score"]
                else:
                    combined_retrieval_result[formula_id] = repr_results_map[formula_id]
        time2 = time.time()
        time_retrieval = time2 - time1
        retrieval_result = \
            dict(
                sorted(
                    combined_retrieval_result.items(),
                    key=lambda item: item[1]["Score"], reverse=True))
        return retrieval_result, time_representation_extraction, time_retrieval
