import os
from search import SearchViaES


def index(sves):
    sves.delete_indies()
    sves.generate_vector_index()

    dir_slt = "Embeddings/SLT/"
    for file in os.listdir(dir_slt):
        sves.load_in_index(dir_slt+file, sves.index_name_slt)

    dir_opt = "Embeddings/OPT/"
    for file in os.listdir(dir_opt):
        sves.load_in_index(dir_opt + file, sves.index_name_opt)

    dir_slt_type = "Embeddings/SLTType/"
    for file in os.listdir(dir_slt_type):
        sves.load_in_index(dir_slt_type + file, sves.index_name_slt_type)

    dir_opt_type = "Embeddings/OPTType/"
    for file in os.listdir(dir_opt_type):
        sves.load_in_index(dir_opt_type + file, sves.index_name_opt_type)


def search(sves, latex_formula_query):
    result = sves.retrival(latex_formula_query)
    return result


def main():
    sves = SearchViaES()
    index(sves)
    search("a+b")

if __name__ == '__main__':
    main()

