import numpy

from ES_module.conversion_tools import tsv_to_elastic_list
from ES_module.elastic_search_interface import ElasticSearchInterFace
from ES_module.search_test import SearchVisESTest


def main():
    search_tool = SearchVisESTest()
    # search_tool.generate_vector_index()
    search_tool.load_in_index("sample_cft.tsv", search_tool.index_name_slt)
    temp = search_tool.retrieval_modified("a+b", 10)
    print(temp)


if __name__ == '__main__':
    main()
