# Author: Behrooz Mansouri (bm3302@rit.edu)
# The goal of this class to manage individual intermediate index TSV files
import csv
import enum


# creating enumerations using class
class FileType(enum.Enum):
    document = 1
    location = 2
    representation = 3
    errors = 4


class FileManager:
    """
    This class is designed to manage individual intermediate index TSV files
    There are 5 intermediate index files: File Names, Content MathML, Presentation MathML, Latex, Formula Location
    """

    def __init__(self, source_dir, current_id, write_limit, file_type):
        """
        @param source_dir: This is the directory in which the related TSV files are written
        @param current_id: This is the file id of the current file in which data is being saved
        @param write_limit: Each TSV file has a limit for the number of records
        """
        self.__source_directory = source_dir
        self.__counter = 0
        self.__current_file_id = current_id
        self.write_limit = write_limit
        self.file_type = file_type
        self.csv_file, self.csv_writer = self.__init_directories()

    def __init_directories(self):
        """
        Prepare the csv writer by creating the csv file setting the number of current records and returning the writer
        @return: the file, and csv writer
        """
        self.__counter = 0
        csv_file = open(self.__source_directory + str(self.__current_file_id) + ".tsv", mode='w')
        if self.file_type is FileType.document:
            lst_header = ["DocumentId", "FilePath"]
        elif self.file_type is FileType.location:
            lst_header = ["FormulaId", "DocumentId", "PageId", "FormulaRegionId", "VisualId", "MinX", "MinY", "MaxX",
                          "MaxY"]
        elif self.file_type is FileType.errors:
            lst_header = ["FormulaId", "Type"]
        else:
            lst_header = ["FormulaId", "VisualId", "Representation"]
        csv_writer = csv.writer(csv_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csv_writer.writerow(lst_header)
        return csv_file, csv_writer

    def write_records(self, lst_records):
        """
        Takes in a list of records and write them in the file
        @param lst_records: list of records to be written to the file
        @return: Empty
        """
        for record in lst_records:
            self.csv_writer.writerow(record)
            self.__counter += 1
        if self.__counter >= self.write_limit:
            self.close()
            self.__current_file_id += 1
            self.csv_file, self.csv_writer = self.__init_directories()

    def close(self):
        """
        When the file limit is reach it is used to close the file
        """
        self.csv_file.close()
