beautifulsoup4==4.10.0
lxml==4.7.1
pandoc==2.0.1
opensearch-py==1.1.0
tqdm==4.64.0
pandas==1.1.4
urllib3==1.26.9
requests==2.25.1
