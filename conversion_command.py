import argparse
import os
from conversion_pipeline import ConversionPipeline


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-dir', type=str,
                        help='root directory of extraction TSV files')
    parser.add_argument('-tar', type=str, help='location to save index files')
    args = vars(parser.parse_args())
    directory = args['dir']
    target = args['tar']
    lst_files = [os.path.join(directory, file) for file in os.listdir(directory)]
    print(len(lst_files))
    cp = ConversionPipeline(target)
    cp.read_extraction_files_and_write_index(lst_files)


if __name__ == '__main__':
    main()
