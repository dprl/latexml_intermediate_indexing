import os
import csv
import sys
import warnings
warnings.filterwarnings("ignore")

from TangentCFT.Embedding_Preprocessing.encoder_tuple_level import TupleTokenizationMode
from TangentCFT.tangent_cft_back_end import TangentCFTBackEnd

temp_dic = {
    "<csymbol cd=\"mws\" name=\"qvar\">qvar_$</csymbol>": "",
    "<csymbol cd=\"latexml\">differential-d</csymbol>": "<ci>d</ci>",
    "<csymbol cd=\"latexml\">conditional</csymbol>": "<ci>c</ci>",
    "<csymbol cd=\"mws\" name=\"qvar\">qvar_</csymbol>": "",
}


def _read_filename_file(source_dir):
    result_dic = {}
    source = source_dir + "/FileNames/"
    for file in os.listdir(source):
        with open(source+file, mode='r') as infile:
            csv_reader = csv.reader(infile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            next(csv_reader)
            for row in csv_reader:
                DocumentId = row[0]
                FilePath = row[1]
                result_dic[DocumentId] = FilePath
    return result_dic


def _read_location_file(file_path):
    result_dic = {}
    with open(file_path, mode='r') as infile:
        csv_reader = csv.reader(infile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        next(csv_reader)
        # FormulaId	DocumentId	PageId	FormulaRegionId	VisualId	MinX	MinY	MaxX	MaxY
        for row in csv_reader:
            FormulaId = row[0]
            other_info = row[1:]
            result_dic[FormulaId] = other_info
    return result_dic


def read_index_tsv_file(file_path):
    """
    Takes in TSV representation file and returns dictionary of (formula id, visual id): representation [SLT/OPT]
    """
    result_dic = {}
    with open(file_path, mode='r') as infile:
        csv_reader = csv.reader(infile, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        next(csv_reader)
        for row in csv_reader:
            formula_id = row[0]
            representation = row[2]
            visual_id = row[1]
            result_dic[(formula_id, visual_id)] = representation
    return result_dic


def extract_embeddings(dic_representation, cft_module):
    """
    Given the dictionary of representation (SLT/OPT) return dictionary of their embeddings
    """
    result_dictionary = {}
    counter = 0
    for formula_visual_id in dic_representation:
        try:
            representation = dic_representation[formula_visual_id]
            for item in temp_dic:
                representation = representation.replace(item, temp_dic[item], representation.count(item))
            embedding = cft_module.get_vector_representation(representation)
            result_dictionary[formula_visual_id] = embedding
        except:
            # print(representation)
            counter += 1
    # print(counter)
    return result_dictionary


def convert_representation_vector(source_dir, location_dir, latex_dir, dic_document_id_path, destination_dir, cft_module):
    """
    Takes in the source directory where intermediate (SLT/OPT) representations are
    The destination directory to save the vectors and the cft module for extracting the vectors
    """
    if not os.path.isdir(destination_dir):
        os.mkdir(destination_dir)
    for file in os.listdir(source_dir):
        dic_representation = read_index_tsv_file(source_dir + file)
        dic_latex_representation = read_index_tsv_file(latex_dir + file)
        dic_formula_id_info = _read_location_file(location_dir + file)
        dic_embeddings = extract_embeddings(dic_representation, cft_module)
        with open(destination_dir + "/" + file, mode='w') as csv_file:
            csv_writer = csv.writer(csv_file, delimiter='\t', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(
                ["FormulaId", "DocumentId", "PageId", "FormulaRegionId", "VisualId", "MinX", "MinY", "MaxX", "MaxY",
                 "Location", "Latex", "formula_vector"])
            for formula_visual_id in dic_embeddings:
                formula_id = formula_visual_id[0]
                lst_to_write = [formula_id]
                temp_lst = dic_formula_id_info[formula_id]
                document_id = temp_lst[0]
                file_path = dic_document_id_path[document_id]
                temp_lst.append(file_path)
                lst_to_write.extend(temp_lst)
                vector = dic_embeddings[formula_visual_id]
                latex = dic_latex_representation[formula_visual_id]
                lst_to_write.append(latex)
                lst_to_write.append(vector)
                csv_writer.writerow(lst_to_write)


def main():
    """
    Given the four trained cft models, this code takes in the intermediate SLT/OPT representations and
    save their vector representations in form of
    """
    representation_dir = sys.argv[1]
    embedding_dir = sys.argv[2]

    if not os.path.isdir(embedding_dir):
        os.mkdir(embedding_dir)
    option = int(sys.argv[3])

    dic_document_id_path = _read_filename_file(representation_dir)

    cft_encoder_file = "TangentCFT/Embedding_Preprocessing/token_ids.tsv"
    formula_location_dir = representation_dir + "/FormulaLocation/"
    formula_latex_dir = representation_dir + "/FormulaLatex/"

    if option == 1:
        config_file = "TangentCFT/Configuration/config/config_102"
        cft_model = "TangentCFT/Saved_Models/a3_slt_102"
        is_slt = True
        tokenization = TupleTokenizationMode.Both_Separated
        number_tokenizer = True
        representation_to_use = representation_dir + "/FormulaPML/"
        embedding_result_path = embedding_dir + "/SLT"
    elif option == 2:
        config_file = "TangentCFT/Configuration/config/config_100"
        cft_model = "TangentCFT/Saved_Models/a3_opt_100"
        is_slt = False
        tokenization = TupleTokenizationMode.Both_Separated
        number_tokenizer = False
        representation_to_use = representation_dir + "/FormulaCML/"
        embedding_result_path = embedding_dir + "/OPT"
    elif option == 3:
        config_file = "TangentCFT/Configuration/config/config_104"
        cft_model = "TangentCFT/Saved_Models/a3_slt_type_104"
        is_slt = True
        tokenization = TupleTokenizationMode.Type
        number_tokenizer = False
        representation_to_use = representation_dir + "/FormulaPML/"
        embedding_result_path = embedding_dir + "/SLTType"
    else:
        config_file = "TangentCFT/Configuration/config/config_105"
        cft_model = "TangentCFT/Saved_Models/a3_opt_type_105"
        is_slt = False
        tokenization = TupleTokenizationMode.Type
        number_tokenizer = False
        representation_to_use = representation_dir + "/FormulaCML/"
        embedding_result_path = embedding_dir+"/OPTType"

    tangent_cft = TangentCFTBackEnd(config_file, read_slt=is_slt, embedding_type=tokenization,
                                    tokenize_number=number_tokenizer)
    tangent_cft.load_model(cft_encoder_file, cft_model)
    convert_representation_vector(representation_to_use, formula_location_dir, formula_latex_dir,
                                  dic_document_id_path, embedding_result_path, tangent_cft)


if __name__ == '__main__':
    main()
